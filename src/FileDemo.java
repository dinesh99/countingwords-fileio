import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class FileDemo {
                public static void main(String[] args) {

                                File f = new File("log.txt");
                                int count = 0;
                                if (!f.exists()) {
                                                try {
                                                                f.createNewFile();
                                                } catch (IOException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                }
                                }

                                try {
                                                FileInputStream fos = new FileInputStream(f);
                                                BufferedReader br = new BufferedReader(new InputStreamReader(fos));

                                                String output;
                                                while ((output = br.readLine()) != null) {
                                                                StringTokenizer st = new StringTokenizer(output);
                                                                while (st.hasMoreTokens()) {
                                                                                String str = st.nextToken();
                                                                                if (str.equalsIgnoreCase("Knowledge"))
                                                                                                count++;
                                                                }
                                                }
                                                br.close();
                                                fos.close();

                                                System.out.println(count);
                                } catch (IOException e) {
                                                System.out.println(e);
                                }

                }
}
